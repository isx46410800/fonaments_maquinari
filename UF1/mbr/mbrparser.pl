#!/usr/bin/perl -w

# Program mbrparser.pl

# (c) Gary C. Kessler, 2011-2013 [gck@garykessler.net]

# Program to parse a DOS/Windows Master Boot Record (MBR)
# Ref: File System Forensic Analysis, Brian Carrier
#  and other documentation

# General structure of the MBR:
#
# Bytes 0-445: Boot code
# Bytes 446-461: Partition table entry #1
# Bytes 462-477: Partition table entry #2
# Bytes 478-493: Partition table entry #3
# Bytes 494-509: Partition table entry #4
# Bytes 510-511: Signature (0x55-AA)


# ********************************************************
# MAIN PROGRAM BLOCK
# ********************************************************

#use Encode qw/encode_utf8 decode_utf8/;
use utf8;
binmode STDOUT, ":utf8";

# Initialize global variables

$version = "2.1";
$build_date = "13 March 2013";

# $source is the name of the file to read. It is expected to be
# a 512-byte file

$source = "";

# boot_flag is an array with the meaning of the active flag

%boot_flag =
  (
  0x00 => "not active",
  0x80 => "active",
  );

# partition_type is an array with a small set of partition type indicator
# values. Ref: http://www.win.tue.nl/~aeb/partitions/partition_types-1.html

%partition_type =
  (
  0x00 => "Unused",
  0x01 => "DOS 12-bit FAT",
  0x04 => "DOS 3.0+ 16-bit FAT (up to 32M)",
  0x05 => "DOS 3.3+ Extended Partition",
  0x06 => "DOS 3.31+ 16-bit FAT (over 32M)",
  0x07 => "Windows NT NTFS or exFAT (aka FAT 64)",
  0x0b => "WIN95 OSR2 FAT32",
  0x0c => "WIN95 OSR2 FAT32, LBA-mapped",
  0x0e => "WIN95: DOS 16-bit FAT, LBA-mapped",
  0x0f => "WIN95: Extended partition, LBA-mapped",
  0x18 => "AST SmartSleep Partition",
  0x27 => "PQservice",
  0x39 => "Plan 9 partition",
  0x3c => "PartitionMagic recovery partition",
  0x44 => "GoBack partition",
  0x51 => "Novell",
  0x52 => "CP/M",
  0x63 => "Unix System V",
  0x64 => "PC-ARMOUR protected partition",
  0x82 => "Solaris x86",
  0x83 => "Linux native partition",
  0x84 => "Hibernation partition",
  0x9f => "BSD/OS",
  0xa0 => "Laptop hibernation partition",
  0xa1 => "Laptop hibernation partition",
  0xa5 => "BSD/386, 386BSD, NetBSD, FreeBSD",
  0xa6 => "OpenBSD",
  0xa7 => "NeXTStep",
  0xa8 => "Mac OS-X",
  0xab => "Mac OS-X Boot partition",
  0xaf => "MacOS X HFS",
  0xb7 => "BSDI BSD/386 filesystem",
  0xb8 => "BSDI BSD/386 swap partition",
  0xbb => "Boot Wizard hidden",
  0xbe => "Solaris 8 boot partition",
  0xd8 => "CP/M-86",
  0xde => "Dell PowerEdge Server utilities (FAT fs)",
  0xdf => "DG/UX virtual disk manager partition",
  0xeb => "BeOS BFS",
  0xee => "Legacy MBR followed by EFI header in sector 1",
  0xef => "Partition containing an EFI file system",
  0xfb => "VMware File System partition",
  0xfc => "VMware Swap partition",
  );

#
# Begin program activity!!
#

print "\nMaster Boot Record Parser V$version - Gary C. Kessler ($build_date)\n\n";

# Parse command for file name and request missing information.


if (parsecommandline ())
  { exit 1; };

print "Source file = $source\n\n";

# Open input ($source) file and stop program if error

open (INFILE, "<:raw", $source)
  or die "Can't open input file \"$source\": $!";
binmode INFILE;

# Start by reading the entire file, which should be 512 bytes

$i = 0;
while (not (eof INFILE) && $i < 512)
  {
  read (INFILE,$byte,1);
  $mbr [$i] = unpack ("C", $byte);
  $i++;
  }

if ($i != 512)
  { die "File is smaller than one sector ($i bytes)\n"; }

# Start by processing the Boot Code (bytes 0-439), which is really just a
# hex/ASCII list.

print "Boot Code (bytes 0-439)\n";

# Print 27 lines of 16 bytes each, followed by one line of 8 bytes

for ($i=0; $i<=26; $i++)
  {
  $bpos = $i * 16;
  set_output ($bpos,16,@mbr);
  }

$bpos += 16;
set_output ($bpos,8,@mbr);
print "\n";

# Bytes 440-443 are the Windows disk signature

$bpos = 440; $len = 4;
printf ("%03d-%03d  Disk signature: ", $bpos,$bpos+$len-1);
list_hex_digits ($bpos, $len, @mbr);
print "\n";

# Bytes 444-445 are generally 0x00-00

$bpos = 444; $len = 2;
printf ("%03d-%03d  (Unused): ", $bpos,$bpos+$len-1);
list_hex_digits ($bpos, $len, @mbr);
print "\n\n";

# Now process the four partition tables. Each PT entry is 16 bytes in length

for ($i=1; $i<=4; $i++)
  {
  $bpos = 446 + ($i-1) * 16;
  set_partition_table ($i,$bpos,@mbr);
  }

# Finally, process the final two-byte signature (should be 0xAA-55)

$bpos = 510; $len = 2;
$temp = &little_endian ($bpos,$len,@mbr);
printf ("%03d-%03d  Signature: ", $bpos,$bpos+$len-1);
list_hex_digits ($bpos,$len,@mbr);

if ($temp == 0xaa55)
    { print " [valid]\n\n"; }
  else
    { print " [invalid]\n\n"; }

# Finish up!

close INFILE;


# ********************************************************
# *****           SUBROUTINE BLOCKS                  *****
# ********************************************************

# ********************************************************
# CHS_translation
# ********************************************************

# Parse three bytes of CHS address and unpack the individual
# C, H, and S values

sub CHS_translation ($@)
{
my ($n,@array) = @_;
my ($temp);

# CHS information is stored in three bytes
# Byte 0 = head (8 bits)
# Byte 1:8-7 + Byte 2 = cylinder (10 bits)
# Byte 1:6-1 = sector (6 bits)

$head = $array [$n];
$sector = $array[$n+1] % 64;
$cylinder = ($array[$n+1] - $sector) * 4 + $array[$n+2];

return;
}

# ********************************************************
# commify
# ********************************************************

# Routine from PerlFAQ5 -- add commas to large integers
# http://perldoc.perl.org/perlfaq5.html
  #How-can-I-output-my-numbers-with-commas-added?

sub commify

{
local $_  = shift;
1 while s/^([-+]?\d+)(\d{3})/$1,$2/;
return $_;
}

# ********************************************************
# help_text
# ********************************************************

# Display the help file

sub help_text
{
print<< "EOT";
Program usage: mbrparser [-i input_file]
               mbrparser {[-h] | [-v] | [-t]}

 where: -i is the input file name
        -h prints this help file
        -v displays the program version number
        -t prints the MBR template file

If no -i switch is provided, the program will ask for the
input file name.

The input file should contain the contents of a Master Boot Record.
The file should be a binary file that is a single sector (512 bytes)
in length. The best way to obtain the MBR is to export the file
from your forensics software.

EOT
return;
}


# ********************************************************
# list_hex_digits
# ********************************************************

# Given an array name, starting position, and number of digits, print
# out a string of hex digits in the format 0xAA-BB-CC...

sub list_hex_digits
{
my ($start,$n,@array) = @_;
my $i;

for ($i = $start; $i < $start+$n; $i++)
  {
  if ($i == $start)
      { print "0x"; }
    else
      { print "-"; }
  printf ("%02X", $array[$i]);
  }

return;
}


# ********************************************************
# little_endian
# ********************************************************

# Given an array name, starting position, and number of digits,
# calculate the decimal value using little endian (LSB first)

sub little_endian ($$@)
{
my ($start,$n,@array) = @_;
my ($i, $sum);

$sum = $array[$start];
for ($i=1; $i<$n; $i++)
  { $sum += $array[$start+$i] * (2**($i*8)); }

return $sum;
}


# ********************************************************
# parsecommandline
# ********************************************************

# Parse command line for file name, if present. Query
# user for any missing information

# Return $state = 1 to indicate that the program should stop
# immediately (switch -h)

sub parsecommandline
{
my $state = 0;

# Parse command line switches ($ARGV array of length $#ARGV)

if ($#ARGV >= 0)

  { 
  for ($i = 0; $i <= $#ARGV; $i++)
    {
    PARMS:
      {
      $ARGV[$i] eq "-i" && do
         {
         $source = $ARGV[$i+1];
         $i++;
         last PARMS;
         };
      $ARGV[$i] eq "-t" && do
         {
         $layout = "layout_MBR.txt";
         open (INFILE, "<", $layout)
          or die "Can't open input file \"$layout\": $!";
         while (not (eof INFILE))
           {
           $line = <INFILE>;
           print $line;
           }
         close INFILE;
         $state = 1;
         return $state;
         };
      $ARGV[$i] eq "-v" && do
         {
         $state = 1;
         return $state;
         };
      $ARGV[$i] eq "-h" && do
         {
         help_text();
         $state = 1;
         return $state;
         };

      do
         {

         print "Invalid parameter \"$ARGV[$i]\".\n\n";
         print "Usage: mbrparser [-i input_file]\n";
         print "       mbrparser {[-h] | [-v] | [-t]}\n\n";
         $state = 1;
         return $state;
         };
      };
    };
  };

# Prompt for file names, if not supplied

if ($source eq "")

  {
  print "Enter the input file name: ";
  chomp ($source = <STDIN>);
  print "\n";
  };

return $state;
}


# ********************************************************
# set_output
# ********************************************************

# Print output of the data bytes to show both hex and ASCII
# values.

sub set_output
{
my ($bpos,$n,@array) = @_;
my ($j, $k, $linemax);

# Format for a maximum of 16 items per line

$linemax = 16;

printf ("%03d: ", $bpos);
for ($j=0; $j<$n; $j++)
  {
  printf (" %02X", $array[$bpos+$j]);
  }
if ($n < $linemax)
  {
  for ($j=1; $j<=$linemax-$n; $j++)
    { print "   "; };
  }

print "   ";
for ($j=0; $j<$n; $j++)
  {
  $k = $array[$bpos+$j];

#   Print a dot (.) for unprintable characters

  if ($k <= 0x1f || ($k >= 0x7f && $k <= 0x9f))
      { print "."; }
    else
      { print chr($k); }
  }
print "\n";

return;
}


# ********************************************************
# set_partition_table
# ********************************************************

# Parse the partition tables.

sub set_partition_table
{
my ($n, $base, @array) = @_;
my ($bos, $j, $k, $len, $linemax);

# Format for a maximum of 16 items per line

print "===== Partition Table #$i =====\n";

# Byte 0 is the Bootable flag (0x80 = active, else 0x00)

$bpos = $base;
printf ("%03d      Boot flag: 0x%02X ", $bpos, $array[$bpos]);
if (defined $boot_flag{$array[$bpos]})
    { print "[$boot_flag{$array[$bpos]}]\n"; }
  else
    { print "(Invalid entry)\n"; }


# Bytes 1-3 contain the starting CHS address of the partition

$bpos = $base + 1; $len = 3;
CHS_translation ($bpos,@mbr);

printf ("%03d-%03d  Starting CHS address: ", $bpos,$bpos+$len-1);
list_hex_digits ($bpos,$len,@array);
print "\n";
print "           Cylinder = $cylinder   Head = $head   Sector = $sector\n";

# Byte 4 is the partition type (per table above)

$bpos = $base + 4;
printf ("%03d      Partition type: 0x%02X ", $bpos, $array[$bpos]);
if (defined $partition_type{$array[$bpos]})
    { print "[$partition_type{$array[$bpos]}]\n"; }
  else
    { print "(Partition type not listed)\n"; }

# Bytes 5-7 contain the ending CHS address of the partition

$bpos = $base + 5; $len = 3;
CHS_translation ($bpos,@mbr);

printf ("%03d-%03d  Ending CHS address: ", $bpos, $bpos+$len-1);
list_hex_digits ($bpos,$len,@array);
print "\n";
print "           Cylinder = $cylinder   Head = $head   Sector = $sector\n";

# Bytes 8-11 contain the starting LBA address (little endian)

$bpos = $base + 8; $len = 4;
$temp = little_endian ($bpos,$len,@array);
printf ("%03d-%03d  Starting LBA address: ", $bpos, $bpos+$len-1);
list_hex_digits ($bpos,$len,@array);
print " [", commify ($temp), "]\n";

# Bytes 12-15 contain the partition size (in sectors)

$bpos = $base + 12; $len = 4;
$temp = little_endian ($bpos,$len,@array);
printf ("%03d-%03d  Partition size: ", $bpos, $bpos+$len-1);
list_hex_digits ($bpos,$len,@array);
print " [", commify ($temp), " sectors]\n\n";

return;
}


