# Exercicis clonació de disc
_Definim la clonació de disc com el procés de fer una imatge d'una partició o d'un disc dur sencer. Les aplicacions directes serien les còpies de seguretat i la recuperació de dades._

*Hi ha moltes eines de clonació. Nosaltres, com sempre estudiarem els clàssics, en aquest cas: dd.*

## Exercise 0
**Aquesta comanda que serveix per copiar i fer conversions (copy/convert) van pensar de dir-li cc però aquest nom ja estava agafat pel compilador de C de manera que van correr la lletra una posició dd. Cert o fals? Investiga.**

+ Falso, La sintaxis fue inspirado por el dd (definición de datos).

## Exercise 1
**Crea un fitxer buit de 650MB amb blocs de 1024KB.**
```
[isx46410800@j02 ~]$ dd if=/dev/zero of=imagen.img bs=1024K count=650 
650+0 records in
650+0 records out
681574400 bytes (682 MB, 650 MiB) copied, 4,75586 s, 143 MB/s
[isx46410800@j02 ~]$ ll -h im*
-rw-r--r--. 1 isx46410800 hisx1 650M abr 10 09:21 imagen.img
```

## Exercise 2
**Crea un fitxer iso d'un cd o dvd.**
```
[root@j02 ~]# dd if=/dev/cdr of=imageniso.iso
```

## Exercise 3
**Crea un backup del primer disc dur, i després de la 2a partició del 2on disc dur.**
```
[root@j02 ~]# dd if=/dev/sda of=backup1.img
[root@j02 ~]# dd if=/dev/sdb2 of=backup2.img
```
**Un cop creats els backups, es podria xequejar la consistència de la imatge dels backups? Si la resposta és afirmativa, com ho faries?**

+ mount -o loop backup1.img /mnt

## Exercise 4
**Quan fem una imatge d'un disc o partició ens podem trobar amb el disc original malmès i, per tant, que provoqui errors. En aquest cas l'ordre dd deixa de llegir i finalitza el programa. Existeix alguna opció perquè l'ordre continui copiant i, per exemple ompli de nulls la resta del bloc que tingui errors i després continui amb el següent?**

+ Opción noerror y sync
        - Noerror: continue after read errors
        - Sync: pad every input block with NULs to ibs-size; when used with block or unblock, pad with spaces rather than NULLS.


## Exercise 5
**Com faries un backup del MBR? i del MBR + la taula de particions? Com ho restauraries? (Millor no ho feu a casa)**

+ Para copiar el MBR:
dd if=/dev/sda of=mbr.img bs=446 count=1

+ Para copiar el MBR y tablas de particiones:
dd if=/dev/sda of=mbr.img bs=512 count=1

+ Para restaurar el MBR:
dd if=mbr.img of=/dev/sda bs=512 count=1

## Exercise 6
**Com ho faries per netejar el disc completament, tot zeros, per exemple?**

[root@j02 ~]# dd if=/dev/zero of=/dev/sda

## Exercise 7
**De quina manera podríem clonar i comprimir una imatge d'una partició? Afegeix alguna opció perquè la còpia vagi més ràpid.**

**Com faries per restaurar després?**

## Exercise 8
**A diferència d'altres eines com clonezilla, Ghost o partimage, dd emmagatzemarà tota la partició incloent, per tant, els blocs lliures. En principi aquest treball extra no és excessiu sempre i quan es comprimeixi la imatge i els blocs lliures no tinguin molta dispersió (partició molt defragmentada).**

**Hi ha una altre problema però, si els blocs lliures prèviament havien estat utilitzats, contindran restes de fitxers eliminats i això implicarà que la compressió no serà trivial.**

**La solució al problema anterior és fàcil però, només cal aconseguir buidar els blocs lliures (que els blocs lliures siguin omplerts per 0's). D'aquesta manera la compressió serà trivial i ocuparà molt menys.**

**Clonarem comprimint de 2 maneres, primer sense utilitzar aquesta tècnica i després fent-la servir:**

+ Crearem una partició d'uns 3 GB. Per a la primera manera podem utilitzar /dev/urandom per omplir tota la partició amb un fitxer ple de brossa i després l'eliminem. Còpiem posteriorment una bona quantitat de fitxers, per exemple 500 MB.

+ Procedim a crear la imatge d'aquesta partició comprimint al mateix temps (poseu l'ordre time davant).

Per a la 2a manera heu d'executar diferents ordres separades per ; representant:

+ Mostro l'espai d'ús de la partició on em trobo ; creo un fitxer ple de zeros que ocupi tot l'espai lliure de la partició; mostro l'espai d'ús de la partició on em trobo ; elimino el fitxer ple de zeros d'abans ; mostro l'espai d'ús de la partició on em trobo.

+ Finalment procedim a crear la imatge d'aquesta partició comprimint al mateix temps (poseu l'ordre time davant).

## Exercise 9
**A la partició /dev/sdb3 d'un pen usb no es poden llegir les dades perquè hi ha sectors defectuosos. Amb quina instrucció podem recuperar tots els sector que no siguin defectuosos?**

+ fsck
+ e2fsck -p -v -y /dev/sdb3

## Exercise 10
**Coneixeu alguna altra manera de comprovar si s'ha fet correctament la imatge d'una partició? (Hint: md5sum)**
mount 

## Exercici 11
**L'ordre dd també permet fer certes conversions curioses. Troba quina seria l'instrucció que et permetria convertir tot a majúscules o a minúscules un cert fitxer.**

+ Mayúsculas:
```
[isx46410800@j02 ~]$ dd if=bebe.txt of=bebemayus.txt conv=ucase
0+1 records in
0+1 records out
370 bytes copied, 0,0204205 s, 18,1 kB/s
[isx46410800@j02 ~]$ cat bebemayus.txt 
BEBE.TXT
BIN
DESKTOP
DOCUMENTS
DOWNLOADS
ERROR.LOG
```
+ Minúsculas:
```
[isx46410800@j02 ~]$ dd if=bebemayus.txt of=bebeminus.txt conv=lcase
0+1 records in
0+1 records out
370 bytes copied, 0,000347315 s, 1,1 MB/s
[isx46410800@j02 ~]$ cat bebeminus.txt 
bebe.txt
bin
desktop
documents
downloads
error.log
```

## Exercici 12
**Tinc la imatge d'una partició, sda2.img, i vull veure el seu contingut, quina ordre em permetrà veure el seu contingut (sense restaurar la partició en un cap disc)?**

+ mount -o loop sda2.img /mnt