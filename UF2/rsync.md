# Exercicis rsync
## Exercici 0
**Què vol dir rsync?**

+ Remote Synchronization

**De quants formes es pot utilitzar rsync?**

+ local(mismo host)
+ remota por ssh
+ remota por rsync

## Exercici 1
**Imiteu el comportament de cp [-a] amb rsync, és a dir feu servir rsync de forma local amb algun directori vostre.**

+ cp -a lo que hace es que al copiar si se corta o algun problema, deja lo que ha podido transferir

**Mireu quina opció us manté parcialment els fitxers que es transfereixen (i per tant en cas de que es deixi de transferir dades sense haver acabat la còpia els fitxers que s'hagin transferit parcialment estiguin ja copiats al directori destí).**

+ Opción --partial/-P

## Exercici 2
**Ara volem que es mostri una barra de progrés durant la transferència.**

+ Opción --progress

## Exercici 3
**Que fa l'opció -a? I l'opció -vv? I -z?**

+ -v: modo verboso, da más información al hacer transferencias.
+ -a: modo de archivo. Copia datos en forma recursiva y mantiene propietarios y permisos.
+ -z: comprime los datos al copiar.
+ -r: es el modo de archivo que permite copiar archivos recursivamente junto con sus permisos de archivos, enlaces simbólicos, etc.

## Exercici 4
**Hi ha alguna opció que faci una execució de prova (un test) sense fer realment la còpia (la sincronització)?**

+ Opción --dry-run/-n

## Exercici 5
**Per defecte s'esborren els fitxers a destí si a l'origen ja no existeixen (per exemple, en una 2a sincronització)?**

+ No se borran los ficheros.
```
_prueba_:
[isx46410800@j02 tmp]$ rsync -r --progress classe/ prova
sending incremental file list
file4.txt
            409 100%    0.00kB/s    0:00:00 (xfr#1, to-chk=0/2)
[isx46410800@j02 tmp]$ ll prova*
total 4
-rw-r--r--. 1 isx46410800 hisx1 409 abr  3 09:40 file4.txt
[isx46410800@j02 tmp]$ rm classe/file4.txt 
[isx46410800@j02 tmp]$ ls > classe/file5.txt
[isx46410800@j02 tmp]$ rsync -r --progress classe/ prova
sending incremental file list
file5.txt
            409 100%    0.00kB/s    0:00:00 (xfr#1, to-chk=0/2)
[isx46410800@j02 tmp]$ ll prova*
total 8
-rw-r--r--. 1 isx46410800 hisx1 409 abr  3 09:40 file4.txt
-rw-r--r--. 1 isx46410800 hisx1 409 abr  3 09:40 file5.txt
[isx46410800@j02 tmp]$ ll classe/
total 4
-rw-r--r--. 1 isx46410800 hisx1 409 abr  3 09:40 file5.txt
```

## Exercici 6
**Són equivalents les instruccions?**

+ NO.

**rsync -r origen desti** --> se copia el directorio entero.

**rsync -r origen/ desti** --> se copian los ficheros del directorio.

```
[isx46410800@j02 tmp]$ rsync -r classe prova
[isx46410800@j02 tmp]$ ll classe
total 4
-rw-r--r--. 1 isx46410800 hisx1 409 abr  3 09:40 file5.txt
[isx46410800@j02 tmp]$ ll prova
total 0
drwxr-xr-x. 2 isx46410800 hisx1 60 abr  3 09:42 classe
[isx46410800@j02 tmp]$ rsync -r classe/ prova
[isx46410800@j02 tmp]$ ll prova
total 4
drwxr-xr-x. 2 isx46410800 hisx1  60 abr  3 09:42 classe
-rw-r--r--. 1 isx46410800 hisx1 409 abr  3 09:43 file5.txt
```

## Exercici 7
**Com indico a l'ordre rsync que vull utilitzar el protocol ssh?**

+ rsync -e ssh origen destino.
```
[root@j02 ~]# rsync -e ssh bebesita.txt j01:~
[isx46410800@j02 ~]$ rsync -e ssh bebe.txt isx46410800@j01:/tmp/
```

+ Para especificar el puerto:
```
[root@j02 ~]# rsync -e "ssh -p 22222" bebesita.txt j01:~
[isx46410800@j02 ~]$ rsync -e "ssh -p 7777"  bebe.txt  isx46410800@j01:/tmp/
```

## Exercici 8
**Treballem com a sysadmins, ja tenim enllestida la configuració de ssh, hem comprovat que rsync està instal·lat a la màquina client i procedeixo a fer una primera sincronització remota. Que obvietat podria haver oblidat, de manera que no es pogués fer la sincronització?**

+ Que esté instalado también en la máquina servidor.

## Exercici 9
**Tot i que a casa o a l'escola els valors/noms siguin diferents, per tenir tots la mateixa resposta a l'exercici, suposarem que tenim dos ordinadors i dos usuaris amb les següents característiques:**

**Servidor que farà el backup**

**hostname: servidor_backup**
**usuari: super_backup**
**directori a on es desa el backup: /opt/backup_rsync_docs/**
**Ordinador que té les dades (de les quals es farà el backup)**

**hostname: ordinador_dades**
**usuari: super_dades**
**directori del qual es fa backup: /opt/docs**
**Potser necessitareu que root crei els directoris adients a /opt i canviï el propietari**

**Quina seria l'ordre que sincronitzaria, és a dir que copiaria el contingut de /opt/docs a /opt/backup_rsync_docs/ de manera que fos una còpia idèntica? Estem suposant que l'ordre l'executem des de servidor_backup. Utilitzem les opcions adients que hem comentat als exercicis anteriors.**
```
[root@j02 backup_rsync_docs]# time rsync -e "ssh -p 7777" j01:/opt/docs/* .
root@j01's password: 

real	0m2.749s
user	0m0.026s
sys	0m0.016s
[root@j02 backup_rsync_docs]# ll
total 4
-rw-r--r--. 1 root root 5 Apr  5 09:55 file{1-50}
```


## Exercici 10
**Si heu fet l'exercici anterior bé se us haurà demanat la contrasenya de l'usuari super_dades al intentar connectar-vos mitjançant ssh. Com que serà una tasca que es repetirà sovint es podria establir una certa confiança.**

**En efect, l'únic que s'ha de fer és generar una parella de claus pública i privada sense contrasenya i guardar-la a .ssh/authorized_keys.**

**A banda de decidir quin user/pc ha de fer segons què, hi ha una ordre força interessant per estalviar-vos la part final: ssh-copy-id**

**1. Creamos las claves privadas y públicas con ssh-keygen en el host**
```
[isx46410800@j02 Documents]$ ssh-keygen 
Generating public/private rsa key pair.
Enter file in which to save the key (/home/users/inf/hisx1/isx46410800/.ssh/id_rsa): 
/home/users/inf/hisx1/isx46410800/.ssh/id_rsa already exists.
Overwrite (y/n)? y
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/users/inf/hisx1/isx46410800/.ssh/id_rsa.
Your public key has been saved in /home/users/inf/hisx1/isx46410800/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:U7adYZeObs2C3jh5NJ8L5LKwQCPwWI0oOztuAbvug6g isx46410800@j02
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|   . o         . |
|. o o .   o o o  |
|.o =     o + *   |
|+.. o o S . = .  |
|.+   o . . =oo   |
|=..   . . oo*oo. |
|=+     . +o*.oo  |
|E+.     . +o. .. |
+----[SHA256]-----+
```

**2. Copiar la clave pública a un equipo remoto con ssh-copy-id**
```
[isx46410800@j02 Documents]$ ssh-copy-id -i /home/users/inf/hisx1/isx46410800/.ssh/id_rsa.pub j03
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/users/inf/hisx1/isx46410800/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
isx46410800@j03's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'j03'"
and check to make sure that only the key(s) you wanted were added.
```

**3. Conexión sin contraseña**
```
[isx46410800@j02 Documents]$ ssh j03
Last login: Fri Mar 29 09:50:42 2019 from 192.168.3.2
```

## EXTRA
**Només teniu un ordinador disponible de manera que no podeu jugar amb dos hosts a la vostra xarxa? Teniu un Android? Llavors si el teniu connectat a la mateixa xarxa que l'ordinador podreu jugar amb ssh i rsync:**

**Tot i que és possible fer servir Google Play, podeu activar la instal·lació de programari de tercers al sistema android i un cop fet això:**

**Instal·lar-se el repositori d'aplicacions lliures f-droid des de [la seva web](https://f-droid.org/)**

**Un cop actualitzats els respositoris de f-droid: instal·lar-se la terminal [termux](https://wiki.termux.com/wiki/Main_Page) des de f-droid.**

**Des de dintre de la terminal termux es poden instal·lar paquets, per exemple:**

  **pkg install openssh  # això instal·larà el servidor i client**
  **pkg install rsync**

**Per engegar el dimoni ssh haureu d'executar des de termux la instrucció sshd i tenir en compte que el port del servidor ssh aquí és el 8022 en comptes de l'habitual 22.**

+ Més informació en aquest [enllaç](http://www.mikerubel.org/computers/rsync_snapshots/)

_Més enllaços:_

+ [Article molt interessant de rsync](http://www.mikerubel.org/computers/rsync_snapshots/)
+ [Un altre article de rsync](https://linuxgazette.net/104/odonovan.html)
+ [rsync ubuntu](https://help.ubuntu.com/community/rsync)
+ [rsync archlinux](https://wiki.archlinux.org/index.php/rsync)